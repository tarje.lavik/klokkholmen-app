<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Klokkholmen</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	
	<!-- Fontawesome -->
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

	<link href="/stylesheets/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Leaflet.js -->
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
    <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
  </head>
  <body>

<?php
error_reporting(0);
include_once("arc/ARC2.php");
include_once("Graphite.php");
$graph = new Graphite();
$graph->load("hyttebok.rdf");
$graph->load("Klokkholmen-metadata.rdf");
$graph->ns("ubbont", "http://data.ub.uib.no/ontology/");
$label=$graph->resource("http://data.klokkholmen.org/hytteboka")->label();
$desc=$graph->resource("http://data.klokkholmen.org/hytteboka")->getString("dct:description");
$resources = $graph->allOfType("ubbont:DigitalResource");
$dzis = $resources->all("ubbont:hasURI")->join("\", \"");
$pages = $graph->allOfType("ubbont:Page");
$dump = $pages->dump();


?>

<!-- Static navbar -->
<div class="navbar navbar-default navbar-static-top" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Klokkholmens hyttebok</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="#map">Kart</a></li>
        <li><a href="#om">Om</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>

<div id="content">

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
<!--
			<div class="page-header">
				<h1><?php print $label;?></h1>
			</div>
-->
			<p class="lead"><?php print $desc;?></p>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-9">
			<div id="dzi">
				<div id="openseadragon"></div>
	            <!-- <div id="navigatorDiv"></div> -->
				 <div id="dzi-control" class="row">
	 			 	<ul class="list-inline">
	 			 		<li id="zoom-out"><button class="btn btn-default"><i class="fa fa-search-minus"></i></button></li>
						<li id="resetZoom"><a class="btn btn-default"><i class="fa fa-arrows-alt"></i> tilpass zoom</a></li>
						<li id="zoom-in"><button class="btn btn-default"><i class="fa fa-search-plus"></i></button></li>
						<li id="full-page"><a class="btn btn-default" alt="Fullskjermvisning av og på"><i id="fullPage" class="fa fa-expand"></i></a></li>
						<li id="previous"><a class="btn btn-default"><i class="fa fa-arrow-left"></i> forrige</a></li>
						<li>gå til side &nbsp;<input class="form-control" type="number" id="page" value="1"/> av <span id="tileSourcesLength"></span></li>
						<li id="next"><a class="btn btn-default">neste <i class="fa fa-arrow-right"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div id="map"></div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<iframe border=”0” class="spreadsheet" src="https://docs.google.com/spreadsheets/d/1fkobD7G0ab26n1K053ELsSyXLLewabExpFwjDxBkscw/edit?usp=sharing"></iframe>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div id="om" class="col-md-12">
				<h2>Om siden</h2>
			<p class="lead">Hytteboka er digitalisert på Avdeling for spesialsamlinger, Universitetsbiblioteket i Bergen. </p>
		</div>
	</div>
</div>

</div>

<div id="footer">
  <div class="container-fluid">
    <p class="text-muted">Klokkholmen v.0.0.1</p>
  </div>
</div>


<script src="node_modules/openseadragon/build/openseadragon/openseadragon.min.js"></script>
<script type="text/javascript">
    var viewer = OpenSeadragon({
        id: "openseadragon",
        prefixUrl: "/openseadragon/images/",
        tileSources: ["<?php print $dzis ?>"],
        showNavigator: true,
		preserveViewport: false,
		minZoomImageRatio: 0.6,
        minPixelRatio: 0.5,
        animationTime: 0.8,
        springStiffness: 8,
        toolbar:      	"dzi-control",
        zoomInButton: 	"zoom-in",
        zoomOutButton:  "zoom-out",
	    homeButton:     "resetZoom",
	    fullPageButton: "full-page",
	    nextButton:     "next",
	    previousButton: "previous"
	    });
	    $('#page').change(function() {
	    	chpage = $('#page').val();
	    	currentPage = chpage-1;
	    	viewer.goToPage(chpage-1);
	    	});
	    viewer.addHandler('page', function(event){
	    	$('#page').val(event.page+1);
	    	chpage = $('#page').val();
	    	});
		function tileSourceLength() {
	    	length = viewer.tileSources.length ;
	    	$('#tileSourcesLength').append(length);
	    	};
	    tileSourceLength();
	    // Under forsoek paa aa lage flip paa ikon for aa gaa inn og ut av fullskjermvisning
	    function fullPage() {
	    	if (viewer.isFullScreen === true) {
		    	$('#fullPage').switchClass('fa fa-compress').addClass('fa fa-expand');
	    	};
	    	if (viewer.fullScreen === '') {
		    	$('#fullPage').removeClass('fa fa-expand').addClass('fa fa-compress');
	    	};
	    	};
	    fullPage();
</script>
<script type="text/javascript">
	map = new L.Map('map');
	// create the tile layer with correct attribution
	var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
	var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 18, attribution: osmAttrib});		
	map.setView(new L.LatLng(60.24188, 5.24438),12);
	var marker = L.marker([60.24188, 5.24438]).addTo(map).bindPopup("<b>Klokkholmen</b><br />Jada, vet at dere vet hvor den er. Har en plan...").openPopup();
	map.addLayer(osm);
	map.scrollWheelZoom.disable();
	//map.touchZoom.disable();
</script>

  </body>
</html>
